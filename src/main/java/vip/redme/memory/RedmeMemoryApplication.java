package vip.redme.memory;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Slf4j
@SpringBootApplication
@EnableJpaRepositories
@EnableJpaAuditing
public class RedmeMemoryApplication {

    public static void main(String[] args) {
        SpringApplication.run(RedmeMemoryApplication.class, args);
    }

}
