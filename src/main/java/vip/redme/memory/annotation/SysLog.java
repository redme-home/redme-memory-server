package vip.redme.memory.annotation;

import java.lang.annotation.*;

/**
 * 业务系统日志注解
 *
 * @author HUZHAOYANG
 * @version 1.0
 * @date 2021/5/14 11:18
 */
@Target({ElementType.PARAMETER, ElementType.METHOD})//作用在参数和方法上
@Retention(RetentionPolicy.RUNTIME)//运行时注解
@Documented//表明这个注解应该被 javadoc工具记录
public @interface SysLog {
    /**
     * 日志描述
     **/
    String desc() default "";

}
