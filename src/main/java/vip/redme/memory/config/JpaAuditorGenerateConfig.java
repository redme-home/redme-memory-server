package vip.redme.memory.config;

/**
 * JpaAuditorGenerateConfig
 *
 * @author HUZHAOYANG
 * @version 1.0
 * @date 2021/9/2 15:38
 */

import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import vip.redme.memory.context.UserContext;

import java.util.Optional;

/**
 * 自动生成jpa中entity的@Createby作者
 */
@Configuration
public class JpaAuditorGenerateConfig implements AuditorAware<String> {

    @Override
    public Optional<String> getCurrentAuditor() {
        return Optional.of(UserContext.getUser().getUserName());
    }
}

