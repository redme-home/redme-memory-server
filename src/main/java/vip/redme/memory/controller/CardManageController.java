package vip.redme.memory.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import vip.redme.memory.enums.ReviewedType;
import vip.redme.memory.exception.BizException;
import vip.redme.memory.pojo.dto.CardDetail;
import vip.redme.memory.pojo.dto.CardView;
import vip.redme.memory.pojo.entity.Card;
import vip.redme.memory.pojo.vo.CardSaveVo;
import vip.redme.memory.response.BaseResultCode;
import vip.redme.memory.response.Result;
import vip.redme.memory.service.CardManageService;

import java.util.List;

/**
 * 卡片管理 controller
 *
 * @author HUZHAOYANG
 * @date 2021/8/30 15:27
 **/
@RestController
@RequestMapping("/card")
public class CardManageController {

    @Autowired
    private CardManageService cardManageService;

    @PostMapping("/create")
    public Result createCard(@RequestBody @Validated CardSaveVo cardSaveVo) {
        cardManageService.createCard(cardSaveVo);
        return Result.success();
    }

    /**
     * 更新Card
     *
     * @return vip.redme.memory.response.Result
     * @author HUZHAOYANG
     * @date 2021/9/2 10:28
     **/
    @PostMapping("/update")
    public Result update(@RequestBody Card card) {
        cardManageService.update(card);
        return Result.success();
    }


    @GetMapping("/delete/{id}")
    public Result deleteCard(@PathVariable("id") long id) {
        cardManageService.deleteCard(id);
        return Result.success();
    }

    /**
     * 根据ID查询一张卡片
     *
     * @param id
     * @return vip.redme.memory.response.Result
     * @author HUZHAOYANG
     * @date 2021/8/30 16:58
     **/
    @GetMapping("/get/{id}")
    public Result getOneCard(@PathVariable("id") long id) {
        Card card = cardManageService.getOneCard(id);
        return Result.success(card);
    }

    /**
     * 查询一个用户下的所有卡片
     *
     * @return vip.redme.memory.response.Result
     * @author HUZHAOYANG
     * @date 2021/8/30 16:58
     **/
    @GetMapping("/get/allCard")
    public Result getAllCard() {
        List<Card> cards = cardManageService.getAll();
        return Result.success(cards);
    }

    /**
     * 根据card id 查询卡片详情
     *
     * @param cardId
     * @return vip.redme.memory.response.Result
     * @author HUZHAOYANG
     * @date 2021/9/2 10:11
     **/
    @GetMapping("/get/cardDetail/{cardId}")
    public Result getOneCardDetail(@PathVariable("cardId") long cardId) {
        CardDetail cardDetail = cardManageService.getOneCardDetail(cardId);
        return Result.success(cardDetail);
    }

    /**
     * 查询一个用户下的所有卡片详情
     *
     * @return vip.redme.memory.response.Result
     * @author HUZHAOYANG
     * @date 2021/8/30 16:58
     **/
    @GetMapping("/get/allCardDetail")
    public Result getAllCardDetail() {
        List<CardDetail> cardDetails = cardManageService.getAllCardDetail();
        return Result.success(CardView.build(cardDetails));
    }

    /**
     * 查询一个用户下的今天应复习卡片
     *
     * @return vip.redme.memory.response.Result
     * @author HUZHAOYANG
     * @date 2021/9/1 11:24
     **/
    @GetMapping("/get/today")
    public Result getTodayItems() {
        List<CardDetail> cardDetails = cardManageService.getAllCardDetail();
        return Result.success(CardView.build(cardDetails).getTodayItems());
    }

    /**
     * 查询一个用户下的未来应复习卡片
     *
     * @return vip.redme.memory.response.Result
     * @author HUZHAOYANG
     * @date 2021/9/1 11:24
     **/
    @GetMapping("/get/plan")
    public Result getPlanItems() {
        List<CardDetail> cardDetails = cardManageService.getAllCardDetail();
        return Result.success(CardView.build(cardDetails).getPlanItems());
    }


    /**
     * 复习了一张卡片
     * 有4种复习选项:
     * 1.认识
     * 2.模糊
     * 3.忘记
     * 4.完全忘记
     *
     * @return vip.redme.memory.response.Result
     * @author HUZHAOYANG
     * @date 2021/9/2 10:55
     **/
    @GetMapping("/reviewed")
    public Result reviewedOneCard(@RequestParam("id") long id, @RequestParam("type") ReviewedType reviewedType) {
        if (id <= 0) {
            throw BizException.of(BaseResultCode.ARGS_ILLEGAL);
        }
        cardManageService.reviewedOneCard(id, reviewedType);
        return Result.success();
    }

    @GetMapping("/delay/{id}")
    public Result delayReview(@PathVariable("id") long id) {
        cardManageService.delayReview(id);
        return Result.success();
    }
}
