package vip.redme.memory.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import vip.redme.memory.pojo.dto.PlanView;
import vip.redme.memory.pojo.entity.Plan;
import vip.redme.memory.pojo.vo.PlanSaveVo;
import vip.redme.memory.response.Result;
import vip.redme.memory.service.PlanManageService;

import java.util.List;
import java.util.stream.Collectors;

/**
 * PlanManageController
 *
 * @author HUZHAOYANG
 * @version 1.0
 * @date 2021/8/31 11:39
 */
@Slf4j
@RestController
@RequestMapping("/plan")
public class PlanManageController {

    @Autowired
    private PlanManageService planManageService;

    @PostMapping("/create")
    public Result createPlan(@RequestBody PlanSaveVo planSaveVo) {
        planManageService.createPlan(planSaveVo);
        return Result.success();
    }

    /**
     * 查询所有Plan
     *
     * @return vip.redme.memory.response.Result
     * @author HUZHAOYANG
     * @date 2021/9/1 16:16
     **/
    @GetMapping("/get/all")
    public Result getAll() {
        List<Plan> plans = planManageService.getAll();
        return Result.success(plans.stream().map(PlanView::from).collect(Collectors.toList()));
    }


}
