package vip.redme.memory.enums;

import lombok.AllArgsConstructor;

/**
 * 复习类型
 *
 * @author HUZHAOYANG
 * @version 1.0
 * @date 2021/9/2 14:12
 */
@AllArgsConstructor
public enum ReviewedType {
    KNOWN {
        @Override
        public int getNextPlanTimes(int currentPlanTimes) {
            return currentPlanTimes + 1;
        }
    },
    UNCLEARLY {
        @Override
        public int getNextPlanTimes(int currentPlanTimes) {
            return currentPlanTimes;
        }
    },
    FORGET {
        @Override
        public int getNextPlanTimes(int currentPlanTimes) {
            currentPlanTimes -= 2;
            currentPlanTimes = Math.max(currentPlanTimes, 1);
            return currentPlanTimes;
        }
    },
    RESET {
        @Override
        public int getNextPlanTimes(int currentPlanTimes) {
            return 1;
        }
    };

    //定义抽象方法
    public abstract int getNextPlanTimes(int currentPlanTimes);

}
