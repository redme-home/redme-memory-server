package vip.redme.memory.exception;

import lombok.Data;
import vip.redme.memory.response.BaseResultCode;
import vip.redme.memory.response.ResultCode;

import java.util.function.Supplier;

/**
 * 业务异常类
 *
 * @author HUZHAOYANG
 * @version 1.0
 * @date 2021/4/22 15:06
 */
@Data
public class BizException extends RuntimeException implements Supplier<BizException> {
    private String code;
    private String message;

    public BizException(String code, String message) {
        super(message);
        this.message = message;
        this.code = code;
    }

    public BizException(String message) {
        this(BaseResultCode.FAIL.getCode(), message);
    }

    public BizException(ResultCode resultCode) {
        this(resultCode.getCode(), resultCode.getMessage());
    }

    public static BizException of(ResultCode resultCode) {
        return new BizException(resultCode);
    }

    /**
     * Gets a result.
     *
     * @return a result
     */
    @Override
    public BizException get() {
        return this;
    }
}
