package vip.redme.memory.pojo.dto;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import lombok.Data;
import lombok.ToString;
import vip.redme.memory.pojo.entity.Card;
import vip.redme.memory.pojo.entity.Plan;
import vip.redme.memory.pojo.entity.Review;
import vip.redme.memory.util.Args;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;

import static vip.redme.memory.pojo.dto.CardView.*;

/**
 * 卡片详情，包括卡片，复习记录，复习计划
 *
 * @author HUZHAOYANG
 * @version 1.0
 * @date 2021/8/30 17:21
 */
@Data
@ToString
public class CardDetail {

    private static final String PLAN_PROCESSING_FORMAT = "{}/{}";

    private Long id;
    /**
     * 标题
     */
    private String title;
    /**
     * 内容
     */
    private String content;
    /**
     * 关联的复习计划
     */
    private Long planId;
    /**
     * user ID
     */
    private Long userId;
    /**
     * 查看次数
     */
    private Integer checkTimes;
    /**
     * 复习总次数
     */
    private Integer reviewedTimes;
    /**
     * 复习计划完成进度
     */
    private String planProcessing;
    /**
     * 复习计划名称
     */
    private String planName;
    /**
     * 应复习时间描述
     */
    private String shouldReviewTimeDesc;

    private String cardDisplayType;

    private Review review;

    public static CardDetail build(Card card, Plan plan, Review review) {
        CardDetail cardDetail = BeanUtil.toBean(card, CardDetail.class);
        cardDetail.setReview(review);
        cardDetail.setPlanName(plan.getName());
        cardDetail.setCardDisplayType(cardDetail.genCardDisplayType());
        cardDetail.setPlanProcessing(StrUtil.format(PLAN_PROCESSING_FORMAT, review.getPlanTimes(), plan.getTotalTimes()));
        cardDetail.transferShouldReviewTimeToDesc();
        return cardDetail;
    }

    public String genCardDisplayType() {
        Args.validateNotNull(review);
        LocalDate shouldReviewDate = review.getShouldReviewTime().toLocalDate();
        LocalDate today = LocalDate.now();
        if (shouldReviewDate.isAfter(today)) {
            //应复习时间是今天之后,则是计划
            return PLAN_ITEMS;
        } else if ((!shouldReviewDate.isAfter(today) && !review.isFinished()) || today.isEqual(review.getFinishedTime().toLocalDate())) {
            // 应复习时间是今天及今天之前,并且未完成复习 || 今天完成的复习
            return TODAY_ITEMS;
        } else {
            return FINISHED_ITEMS;
        }
    }

    public void transferShouldReviewTimeToDesc() {
        String displayType = genCardDisplayType();
        switch (displayType) {
            case FINISHED_ITEMS:
                this.setShouldReviewTimeDesc("已完成");
                break;
            case TODAY_ITEMS: {
                String desc = greatDesc();
                if (getReview().getShouldReviewTime().isAfter(LocalDateTime.now())) {
                    this.setShouldReviewTimeDesc(desc + "后");
                } else {
                    this.setShouldReviewTimeDesc(desc + "前");
                }
                break;
            }
            case PLAN_ITEMS: {
                String desc = greatDesc();
                this.setShouldReviewTimeDesc(desc + "后");
                break;
            }
            default:
                break;
        }
    }

    public String greatDesc() {
        LocalDateTime shouldReviewTime = this.review.getShouldReviewTime();
        LocalDateTime now = LocalDateTime.now();
        Duration duration = Duration.between(now, shouldReviewTime).abs();
        long days = duration.toDays();
        long hours = duration.toHours();
        long minutes = duration.toMinutes();
        if (hours < 1) {
            return minutes + "分";
        } else if (days < 1) {//1天以内
            return hours + "小时";
        } else if (days < 7) {
            return days + "天";
        } else if (days < 31) {
            return days / 7 + "周";
        } else if (days < 365) {
            return days / 30 + "月";
        } else {
            return days / 365 + "年";
        }
    }

}
