package vip.redme.memory.pojo.dto;

import lombok.Data;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * CardView
 *
 * @author HUZHAOYANG
 * @version 1.0
 * @date 2021/9/1 10:17
 */
@Data
public class CardView {
    static final String PLAN_ITEMS = "planItems";
    static final String TODAY_ITEMS = "todayItems";
    static final String FINISHED_ITEMS = "finishedItems";
    private List<CardDetail> AllItems;
    private List<CardDetail> todayItems;
    private List<CardDetail> planItems;

    public static CardView build(List<CardDetail> AllItems) {
        CardView cardView = new CardView();
        cardView.setAllItems(AllItems);
        Map<String, List<CardDetail>> itemsMap = AllItems.stream().collect(Collectors.groupingBy(CardDetail::genCardDisplayType));
        cardView.setTodayItems(Optional.ofNullable(itemsMap.get(TODAY_ITEMS)).orElse(Collections.emptyList()));
        cardView.setPlanItems(Optional.ofNullable(itemsMap.get(PLAN_ITEMS)).orElse(Collections.emptyList()));
        return cardView;
    }
}
