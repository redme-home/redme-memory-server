package vip.redme.memory.pojo.dto;

import cn.hutool.core.bean.BeanUtil;
import lombok.Data;
import vip.redme.memory.pojo.entity.Plan;

import java.util.List;

/**
 * 复习计划选择器展示
 *
 * @author HUZHAOYANG
 * @version 1.0
 * @date 2021/9/1 16:23
 */
@Data
public class PlanView {

    /**
     * user ID
     */
    private Long id;
    /**
     * 复选择器展示名称
     */
    private String label;
    private String value;
    /**
     * 总共需要复习的次数
     */
    private int totalTimes;
    /**
     * 总共需要复习的时间
     */
    private String planPeriod;

    /**
     * 复习计划里程碑,每个里程碑之间的单位为小时数
     * eg: [0，5,12,36] 表示
     * 第0次复习为  创建卡片的时间+ 0(schedule[0]) 小时
     * 第一次复习为 第0次复习完成时间 + 5(schedule[1]) 小时
     * 第二次复习为 第1次复习完成时间 + 12(schedule[2]) 小时
     * 第三次复习为 第2次复习完成时间 + 36(schedule[3]) 小时
     */
    private List<Integer> schedule;

    public static PlanView from(Plan plan) {
        PlanView planView = BeanUtil.toBean(plan, PlanView.class);
        planView.setLabel(plan.getName());
        planView.setValue(plan.getName());
        return planView;
    }

}
