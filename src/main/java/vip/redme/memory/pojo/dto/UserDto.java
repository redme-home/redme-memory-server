package vip.redme.memory.pojo.dto;

import lombok.Data;

/**
 * UserDto
 *
 * @author HUZHAOYANG
 * @version 1.0
 * @date 2021/9/5 13:39
 */
@Data
public class UserDto {

    private Long id;

    private String userName;

    private String type;

    private String token;
}
