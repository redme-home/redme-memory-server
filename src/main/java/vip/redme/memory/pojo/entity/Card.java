package vip.redme.memory.pojo.entity;

import lombok.Data;
import vip.redme.memory.pojo.entity.base.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Card
 * 用户创建的卡片，记录想要记忆的内容
 *
 * @author HUZHAOYANG
 * @version 1.0
 * @date 2021/8/12 20:47
 */
@Data
@Entity
@Table(name = "card")
public class Card extends BaseEntity {

    /**
     * 标题
     */
    private String title;
    /**
     * 内容
     */
    private String content;
    /**
     * 关联的复习计划
     */
    private Long planId;
    /**
     * user ID
     */
    private Long userId;
    /**
     * 查看次数
     */
    private Integer checkTimes;
    /**
     * 复习总次数
     */
    private Integer reviewedTimes;

}
