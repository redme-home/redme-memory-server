package vip.redme.memory.pojo.entity;

import com.vladmihalcea.hibernate.type.json.JsonStringType;
import lombok.Data;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import vip.redme.memory.pojo.entity.base.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.List;

/**
 * Plan
 * 复习计划
 *
 * @author HUZHAOYANG
 * @version 1.0
 * @date 2021/8/12 21:32
 */
@Entity
@Data
@Table(name = "plan")
@TypeDef(name = "json", typeClass = JsonStringType.class)
public class Plan extends BaseEntity {
    /**
     * 复习计划名称
     */
    private String name;
    /**
     * 总共需要复习的次数
     */
    private int totalTimes;
    /**
     * 总共需要复习的时间
     */
    private String planPeriod;

    /**
     * 复习计划里程碑,每个里程碑之间的单位为小时数
     * eg: [0，5,12,36] 表示
     * 第1次复习为  创建卡片的时间 小时
     * 第2次复习为 第1次复习完成时间 + 5(schedule[1]) 小时
     * 第3次复习为 第2次复习完成时间 + 12(schedule[2]) 小时
     * 第4次复习为 第3次复习完成时间 + 36(schedule[3]) 小时
     */
    @Type(type = "json")
    @Column(columnDefinition = "json")
    private List<Integer> schedule;

    /**
     * user ID
     */
    private Long userId;


}
