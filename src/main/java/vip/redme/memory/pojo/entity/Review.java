package vip.redme.memory.pojo.entity;

import lombok.Data;
import lombok.experimental.Accessors;
import vip.redme.memory.enums.ReviewedType;
import vip.redme.memory.pojo.entity.base.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * Review
 * 复习记录
 *
 * @author HUZHAOYANG
 * @version 1.0
 * @date 2021/8/12 21:10
 */
@Entity
@Data
@Table(name = "review")
@Accessors(chain = true)
public class Review extends BaseEntity {
    /**
     * 卡片ID
     **/
    private Long cardId;
    /**
     * user ID
     */
    private Long userId;
    /**
     * 这次复习计划的第几次复习
     **/
    private Integer planTimes;
    /**
     * 是否已完成
     **/
    @Column(nullable = false, columnDefinition = "TINYINT", length = 1)
    private boolean isFinished;
    /**
     * 应该复习的时间
     **/
    @NotNull
    private LocalDateTime shouldReviewTime;
    /**
     * 本次完成复习的时间
     **/
    private LocalDateTime finishedTime;
    /**
     * 是否最新记录(每张卡片都有数条记录,每复习一次增加一条记录
     **/
    @Column(nullable = false, columnDefinition = "TINYINT", length = 1)
    private boolean isLatest;

    /**
     * 本次完成复习的情况: 0-认识,1-模糊,2-忘记,3-完全忘记
     **/
    @Column(columnDefinition = "TINYINT")
    private ReviewedType reviewedType;
    /**
     * 应该复习的时间描述（=当前时间-shouldReviewTime）比如：1天前，2天后
     * 不用保存在数据库，由前端计算渲染
     **/
//    private String shouldReviewTimeDesc;
    /**
     * 推迟详情,用json记录：[{times:1,shouldReviewTime:2021-08-02,delayTo:2021-08-03},{times:2,shouldReviewTime:2021-08-03,delayTo:2021-08-04}]
     **/
//    private List<DelayDetail> delayDetail;


//    @Data
//    public class DelayDetail {
//        private int times;
//        private LocalDateTime shouldReviewTime;
//        private LocalDateTime delayTo;
//    }

}
