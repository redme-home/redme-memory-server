package vip.redme.memory.pojo.entity;

import lombok.Data;
import lombok.experimental.Accessors;
import vip.redme.memory.pojo.entity.base.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "user")
public class User extends BaseEntity {

    private String userName;

    private String password;

    private String type;

    private String token;

}
