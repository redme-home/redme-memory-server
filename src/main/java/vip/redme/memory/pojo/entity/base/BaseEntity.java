package vip.redme.memory.pojo.entity.base;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * TODO
 *
 * @author HUZHAOYANG
 * @version 1.0
 * @date 2021/4/22 10:39
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public class BaseEntity {

    public static String[] IGNORE_PROPERTIES = new String[]{"id", "createBy", "createTime", "updateBy", "updateTime", "version"};

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @CreatedBy
    private String createBy;
    @CreatedDate
    private LocalDateTime createTime;
    @LastModifiedBy
    private String updateBy;
    @LastModifiedDate
    private LocalDateTime updateTime;
    @Version
    private int version = 1;

}
