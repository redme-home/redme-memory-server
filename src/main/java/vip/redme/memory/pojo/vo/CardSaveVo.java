package vip.redme.memory.pojo.vo;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 保存一张卡片
 *
 * @author HUZHAOYANG
 * @version 1.0
 * @date 2021/8/31 11:12
 */
@Data
public class CardSaveVo {
    /**
     * 标题
     */
    @NotBlank
    private String title;
    /**
     * 内容
     */
    @NotBlank
    private String content;
    /**
     * 关联的复习计划
     */
    @NotNull
    private Long planId;

}
