package vip.redme.memory.pojo.vo;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * Plan
 * 复习计划
 *
 * @author HUZHAOYANG
 * @version 1.0
 * @date 2021/8/12 21:32
 */
@Data
public class PlanSaveVo {
    /**
     * 复习计划名称
     */
    @NotBlank
    private String name;

    /**
     * 复习计划里程碑,每个里程碑之间的单位为小时数
     * eg: [0，5,12,36] 表示
     * 第0次复习为  创建卡片的时间+ 0(schedule[0]) 小时
     * 第一次复习为 第0次复习完成时间 + 5(schedule[1]) 小时
     * 第二次复习为 第1次复习完成时间 + 12(schedule[2]) 小时
     * 第三次复习为 第2次复习完成时间 + 36(schedule[3]) 小时
     */
    @Size(min = 1)
    private List<Integer> schedule;

}
