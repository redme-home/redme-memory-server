package vip.redme.memory.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import vip.redme.memory.pojo.entity.Card;

import java.util.List;

/**
 * CardRepository
 *
 * @author HUZHAOYANG
 * @version 1.0
 * @date 2021/8/30 15:37
 */
@Repository
public interface CardRepository extends JpaRepository<Card, Long> {

    List<Card> findByUserId(long userId);

}
