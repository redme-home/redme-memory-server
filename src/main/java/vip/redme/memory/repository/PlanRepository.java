package vip.redme.memory.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import vip.redme.memory.pojo.entity.Plan;

import java.util.List;

/**
 * CardRepository
 *
 * @author HUZHAOYANG
 * @version 1.0
 * @date 2021/8/30 15:37
 */
@Repository
public interface PlanRepository extends JpaRepository<Plan, Long> {

    List<Plan> findByUserId(long userId);

}
