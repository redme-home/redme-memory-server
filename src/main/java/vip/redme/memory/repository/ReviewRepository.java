package vip.redme.memory.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vip.redme.memory.pojo.entity.Review;

import java.util.List;

/**
 * CardRepository
 *
 * @author HUZHAOYANG
 * @version 1.0
 * @date 2021/8/30 15:37
 */
@Repository
public interface ReviewRepository extends JpaRepository<Review, Long> {

    List<Review> findByUserId(long userId);

    List<Review> findByCardId(long cardId);

    @Query(nativeQuery = true, value = "select * from review where userId=:userId and isLatest=1")
    List<Review> findAllCardAfterDate(long userId);

    List<Review> findByUserIdAndIsLatest(long userId, boolean isLatest);

    Review findByCardIdAndIsLatest(long cardId, boolean isLatest);

    @Transactional
    void deleteByCardId(long cardId);


}
