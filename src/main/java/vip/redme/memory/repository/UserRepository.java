package vip.redme.memory.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import vip.redme.memory.pojo.entity.User;

/**
 * CardRepository
 *
 * @author HUZHAOYANG
 * @version 1.0
 * @date 2021/8/30 15:37
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    User findByUserNameAndPassword(String userName, String password);

    User findByToken(String token);

}
