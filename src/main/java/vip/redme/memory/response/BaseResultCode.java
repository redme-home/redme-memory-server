package vip.redme.memory.response;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * TODO
 *
 * @author HUZHAOYANG
 * @version 1.0
 * @date 2021/4/20 16:49
 */
@AllArgsConstructor
public enum BaseResultCode implements ResultCode {

    SUCCESS("200", "成功"),
    FAIL("500", "系统开小差啦，请稍后再试..."),
    ARGS_ILLEGAL("601", "访问参数非法"),
    RECORD_NOT_EXIST("602", "记录不存在！"),
    NO_PERMISSION_ERROR("999", "没有权限访问接口！"),

    ;


    @Getter
    private String code;
    @Getter
    private String message;
}
