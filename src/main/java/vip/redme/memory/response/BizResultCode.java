package vip.redme.memory.response;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 业务返回码
 *
 * @author HUZHAOYANG
 * @version 1.0
 * @date 2021/4/22 15:19
 */
@AllArgsConstructor
public enum BizResultCode implements ResultCode {
    NO_PERMISSION_ERROR("999", "没有权限访问接口！"),

    ;

    @Getter
    private String code;
    @Getter
    private String message;
}
