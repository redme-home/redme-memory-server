package vip.redme.memory.response;

import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;

/**
 * @ClassName ReturnObject
 * @Description 返回结果包装类
 * @Author HUZHAOYANG
 * @Date 2019/8/18 15:26
 * @Version 1.0
 */
@Data
@RequiredArgsConstructor(staticName = "of")
@Accessors(chain = true)
public class Result<T> {

    @NonNull
    private String code;
    @NonNull
    private String message;

    private T data;

    public Result(ResultCode resultCode) {
        this.code = resultCode.getCode();
        this.message = resultCode.getMessage();
    }

    public static Result success() {
        return new Result(BaseResultCode.SUCCESS);
    }

    @SuppressWarnings("unchecked")
    public static <T> Result success(T data) {
        return new Result(BaseResultCode.SUCCESS).setData(data);
    }

    public static Result fail() {
        return new Result(BaseResultCode.FAIL);
    }

    public static Result fail(ResultCode resultCode) {
        return new Result(resultCode);
    }
}
