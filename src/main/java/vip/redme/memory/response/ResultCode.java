package vip.redme.memory.response;

/**
 * TODO
 *
 * @author HUZHAOYANG
 * @version 1.0
 * @date 2021/4/20 16:59
 */
public interface ResultCode {

    String getCode();

    String getMessage();
}
