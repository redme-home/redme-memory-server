package vip.redme.memory.response;

import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * @ClassName ReturnObject
 * @Description 返回结果包装类
 * @Author HUZHAOYANG
 * @Date 2019/8/18 15:26
 * @Version 1.0
 */
@Data
@RequiredArgsConstructor(staticName = "of")
@Accessors(chain = true)
public class ResultPage<T> {

    @NonNull
    private String code;
    @NonNull
    private String message;
    //每页记录数
    private Long size;
    //当前页
    private Long current;
    //总记录数
    private Long total;
    //总页数
    private Long totalPages;
    //数据
    private List<T> data;

    public ResultPage(ResultCode resultCode) {
        this.code = resultCode.getCode();
        this.message = resultCode.getMessage();
    }

    public static ResultPage success() {
        return new ResultPage(BaseResultCode.SUCCESS);
    }

    public static ResultPage fail() {
        return new ResultPage(BaseResultCode.FAIL);
    }

    public static ResultPage fail(ResultCode resultCode) {
        return new ResultPage(resultCode);
    }
}
