package vip.redme.memory.service;

import vip.redme.memory.pojo.dto.UserDto;
import vip.redme.memory.pojo.entity.User;

/**
 * AuthService
 *
 * @author HUZHAOYANG
 * @version 1.0
 * @date 2021/9/5 13:38
 */
public interface AuthService {
    UserDto login(User user);

    UserDto loginByToken(String token);

    UserDto register(User user);
}
