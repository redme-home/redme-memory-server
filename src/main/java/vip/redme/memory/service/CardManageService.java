package vip.redme.memory.service;

import vip.redme.memory.enums.ReviewedType;
import vip.redme.memory.pojo.dto.CardDetail;
import vip.redme.memory.pojo.entity.Card;
import vip.redme.memory.pojo.vo.CardSaveVo;

import java.util.List;

/**
 * CardManage
 *
 * @author HUZHAOYANG
 * @version 1.0
 * @date 2021/8/30 15:33
 */
public interface CardManageService {

    /**
     * 保存一张卡片
     *
     * @param cardSaveVo
     * @return void
     * @author HUZHAOYANG
     * @date 2021/8/31 11:23
     **/
    void createCard(CardSaveVo cardSaveVo);

    /**
     * 删除Card
     *
     * @param id
     * @return void
     * @author HUZHAOYANG
     * @date 2021/9/3 15:15
     **/
    void deleteCard(long id);

    /**
     * 根据ID查询一张卡片
     *
     * @param id
     * @return vip.redme.memory.response.Result
     * @author HUZHAOYANG
     * @date 2021/8/30 16:58
     **/
    Card getOneCard(long id);

    /**
     * 查询一个用户下的所有卡片
     *
     * @return vip.redme.memory.response.Result
     * @author HUZHAOYANG
     * @date 2021/8/30 16:58
     **/
    List<Card> getAll();

    /**
     * 查询一个用户下的所有卡片及其详情
     *
     * @return java.util.List<vip.redme.memory.pojo.dto.CardDetail>
     * @author HUZHAOYANG
     * @date 2021/8/30 17:25
     **/
    List<CardDetail> getAllCardDetail();

    /**
     * 根据card id 查询卡片详情
     *
     * @param cardId
     * @return vip.redme.memory.pojo.dto.CardDetail
     * @author HUZHAOYANG
     * @date 2021/9/2 10:12
     **/
    CardDetail getOneCardDetail(long cardId);

    /**
     * 更新Card
     *
     * @param card
     * @return void
     * @author HUZHAOYANG
     * @date 2021/9/2 10:29
     **/
    void update(Card card);

    /**
     * 复习了一张卡片
     *
     * @param id
     * @param reviewedType
     * @return void
     * @author HUZHAOYANG
     * @date 2021/9/2 11:09
     **/
    void reviewedOneCard(long id, ReviewedType reviewedType);


    /**
     * 推迟复习
     *
     * @param id
     * @return void
     * @author HUZHAOYANG
     * @date 2021/9/3 16:00
     **/
    void delayReview(long id);
}
