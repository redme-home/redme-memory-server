package vip.redme.memory.service;

import vip.redme.memory.pojo.entity.Plan;
import vip.redme.memory.pojo.vo.PlanSaveVo;

import java.util.List;

/**
 * PlanManageService
 *
 * @author HUZHAOYANG
 * @version 1.0
 * @date 2021/8/31 11:45
 */
public interface PlanManageService {
    /**
     * 创建Plan
     *
     * @param planSaveVo
     * @return void
     * @author HUZHAOYANG
     * @date 2021/8/31 14:02
     **/
    void createPlan(PlanSaveVo planSaveVo);

    /**
     * 查询所有Plan
     *
     * @return java.util.List<vip.redme.memory.pojo.entity.Plan>
     * @author HUZHAOYANG
     * @date 2021/9/1 16:16
     **/
    List<Plan> getAll();

}
