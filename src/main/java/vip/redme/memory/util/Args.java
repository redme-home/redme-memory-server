package vip.redme.memory.util;

import vip.redme.memory.exception.BizException;
import vip.redme.memory.response.ResultCode;

import java.util.Collection;

/**
 * 参数校验工具类
 *
 * @author HUZHAOYANG
 * @version 1.0
 * @date 2020/3/28 23:25
 */
public class Args {

    // don't instantiate
    private Args() {
    }

    public static boolean isNull(Object x) {
        return null == x;
    }

    public static boolean notNull(Object x) {
        return !isNull(x);
    }

    public static boolean isEmpty(Object x) {
        if (isNull(x)) {
            return true;
        } else if (x instanceof String) {
            return ((String) x).isEmpty();
        } else if (x instanceof Collection) {
            return ((Collection) x).isEmpty();
        } else {
            return false;
        }
    }

    public static boolean notEmpty(Object x) {
        return !isEmpty(x);
    }

    // throw an IllegalArgumentException if x is null
    // (x can be of type Object[], double[], int[], ...)
    public static void validateNotNull(Object x) {
        if (isNull(x)) {
            throw new IllegalArgumentException("argument is null!");
        }
    }

    /**
     * 如果condition为true，则招聘resultCode异常
     *
     * @param condition
     * @param resultCode
     * @return void
     * @author HUZHAOYANG
     * @date 2021/5/18 9:13
     **/
    public static void check(boolean condition, ResultCode resultCode) {
        if (condition) {
            throw new BizException(resultCode);
        }
    }

}
